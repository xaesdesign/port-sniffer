use std::env;
use std::str::FromStr;
use std::net::{ IpAddr };

const HELP_MESSAGE: &str = r#"
Usage:
    -j to select how many threads you want.
    -h or --help to show this help message

List of possible args:
    port-sniffer -h
    port-sniffer -j <thread_count> <ip_addr>
    port-sniffer <ip_addr>
"#;

pub struct Arguments {
    pub flag: Option<String>,
    pub ip_addr: Option<IpAddr>,
    pub threads: u16
}

impl Arguments {

    // List of possible args:
    //      - <program> -h
    //      - <program> -j <thread_count> <ip_addr>
    //      - <program> <ip_addr>

    pub fn get() -> Result<Arguments, &'static str>{

        // Getting Args from Environment.

        let args: Vec<String> = env::args().collect::<Vec<String>>();

        // Checking if parameters sent are between the max / min allowed.

        if args.len() < 2 || args.len() > 4 {
            return Err("Too many or not enough parameters.");
        }

        if let Ok(ip_addr) = IpAddr::from_str(&args[1]) {
            Ok(Arguments { ip_addr: Some(ip_addr), flag: None, threads: 4 })
        } else {
            
            let flag: String = args[1].clone();

            if flag == "-h" || flag == "--help" && args.len() == 2 {
                println!("{}", HELP_MESSAGE);
                Ok(Arguments { ip_addr: None, flag: Some(flag), threads: 4 })
            } else if flag == "-j" && args.len() > 2 {
                let threads = match args[2].parse::<u16>() {
                    Ok(t) => { t }
                    Err(_) => return Err("Not a valid thread number.")
                };

                let ip_addr = match IpAddr::from_str(&args[3]) {
                    Ok(i) => { i }
                    Err(_) => return Err("Not a valid IP Address.")
                };

                Ok(Arguments { ip_addr: Some(ip_addr), threads, flag: Some(flag) })
            } else { Err("Invalid Syntax.") }
            
        }

    }

}