mod arguments;

use std::{ process, io, thread };
use arguments::Arguments;
use std::sync::mpsc::{ Sender, channel };
use std::net::{ IpAddr, TcpStream, SocketAddr };
use std::io::{ Write };
use std::time::Duration;

const MAX: u16 = 65535;

fn scan(sender: Sender<u16>, start_port: u16, addr: IpAddr, num_threads: u16) {

    let mut port: u16 = start_port + 1;

    loop {

        let socket_address = SocketAddr::new(addr, port);
        let duration = Duration::from_secs(1);

        if TcpStream::connect_timeout(&socket_address, duration).is_ok() {
            print!(".");
            io::stdout().flush().unwrap();
            sender.send(port).unwrap();
        }

        if (MAX - port) <= num_threads { break }
        port += num_threads;

    }

}

fn main() {

    let arguments = match Arguments::get() {
        Ok(arg) => {

            // Terminate if user ask for help function.

            if let Some(flag) = arg.flag.clone() {
                if flag == "-h" || flag == "--help" { process::exit(0); }
            }

            arg

        },
        Err(err) => {
            eprintln!("There was a problem parsing the arguments: {}", err);
            process::exit(0);
        }
    };

    let (sender, receiver) = channel::<u16>();

    // Dispatching Sender Channels across threads.

    let num_threads = arguments.threads;
    let addr = arguments.ip_addr.unwrap();

    for t in 0..arguments.threads {
        let sender = sender.clone();
        thread::spawn(move || scan(
            sender,
            t,
            addr,
            num_threads
        ));
    }

    // Dropping sender.

    drop(sender);

    // Pushing each value on receiver to a vector.

    let mut available_ports: Vec<u16> = Vec::new();
    available_ports.extend(receiver.iter());
    available_ports.sort();

    // Printing Results.

    println!();
    available_ports.iter().for_each(|port| println!("Port ({}) is open", port))

}
