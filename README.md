# Port Sniffer

A Rust CLI tool to check what ports are open given a IP Address.

## Arguments:
- `-j` to select how many threads you want.
- `-h` or `--help` to show a help message.

## Examples:
- `port-sniffer -h`
- `port-sniffer -j <thread_count> <ip_addr>`
- `port-sniffer <ip_addr>`